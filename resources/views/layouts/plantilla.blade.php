<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	
	<!-- favicom -->
	<!-- estilos -->
	<style>
		.active{
			color: red;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<!-- header -->
	<!-- nav -->

	@include('layouts.partials.header')

	@yield('content')

	<!-- footer -->
	@include('layouts.partials.footer')
	
	<!-- script -->

</body>
</html>
